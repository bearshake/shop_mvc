-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 14 2018 г., 09:21
-- Версия сервера: 5.7.19
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shop_mvc`
--
CREATE DATABASE IF NOT EXISTS `shop_mvc` DEFAULT CHARACTER SET utf8 COLLATE utf8_hungarian_ci;
USE `shop_mvc`;

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `sort_order`, `status`) VALUES
(1, 'Shooter', 0, 1),
(2, 'Racing', 0, 1),
(3, 'Fighting', 0, 1),
(7, 'Indie', 0, 1),
(9, 'Simulator', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `price` float NOT NULL,
  `availability` int(11) NOT NULL,
  `firm` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `is_new` int(11) NOT NULL DEFAULT '0',
  `is_recommended` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `code`, `price`, `availability`, `firm`, `image`, `description`, `is_new`, `is_recommended`, `status`) VALUES
(1, 'PUBG', 1, 22, 59.99, 10, 'BlueWhole', 'pubg.jpg', 'PlayerUnknowns:Battlegrounds', 1, 1, 1),
(2, 'CS:GO', 1, 1, 12.65, 112, 'Valve', 'csgo.jpg', 'Counter-strike:Global Offensive', 1, 0, 1),
(3, 'The Bind of IIsac', 7, 2, 5.99, 14, 'IsCorp', 'tbi.jpg', 'The Bind of IIsac Indie game', 0, 1, 1),
(4, 'Battlerite', 3, 13, 0, 3000, 'Valve', 'battlerite.jpg', 'New 2018 GAME\r\nMMO FIGHT', 1, 1, 1),
(5, 'CS:Source', 1, 15, 2, 1, 'Valve Corp.', 'cssource.jpg', 'Counter - strike Source', 0, 0, 1),
(6, 'Battlefield 1', 1, 11, 41.99, 25, 'Dice', 'bf1.jpg', 'Battlefield best of the series about First world war', 1, 1, 1),
(7, 'Battlefield 4', 1, 10, 10, 2, 'Dice ', 'bf4.jpg', 'BF 4 2014 ', 0, 1, 1),
(8, 'Battlefield 3', 1, 9, 10.99, 15, 'Dice', 'bf3.jpg', 'Battlefield 3 DICE CORP.', 0, 1, 1),
(9, 'Grand Turismo Racing', 2, 87, 15.66, 3, 'GTR', 'gtr', 'Grand Turismo 2016 Racing game', 1, 0, 1),
(10, 'Need For Speed: Most Wanted', 2, 23, 7.99, 30, 'Ea', 'nfsmw2.jpg', 'Racing game BEST', 0, 1, 1),
(11, 'Left 4 Dead', 1, 5, 19.99, 23, 'Valve Corp.', 'l4d.jpg', 'Zombie Survival', 0, 0, 1),
(12, 'Left 4 Dead 2', 1, 21, 17.22, 10, 'Valve Corp.', 'l4d2.jpg', 'Survival Zombie', 0, 1, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
