<?php
/**
 * Created by PhpStorm.
 * User: BearShake
 * Date: 03.04.2018
 * Time: 17:50
 */

return array(

    'product/([0-9]+)' => 'product/view/$1',

	'catalog' => 'catalog/index',

	'category/([0-9]+)/page-([0-9]+)' => 'catalog/category/$1/$2',
	'category/([0-9]+)' => 'catalog/category/$1',

	'user/register' => 'user/register',

	'' => 'site/index',
);