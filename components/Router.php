 <?php
/**
 * Created by PhpStorm.
 * User: BearShake
 * Date: 03.04.2018
 * Time: 17:41
 */

class Router {

	private $routes; //array


	public function __construct()
	{
		$routesPath = ROOT.'/config/routes.php';
		$this->routes = include($routesPath);
	}

	/**
	 * Returns req str
	 * @return string
	 */

	private function getURL()
	{
		if(!empty($_SERVER['REQUEST_URI'])){
			return trim($_SERVER['REQUEST_URI'], '/');
		}
	}

	public function run()
	{
		$url = $this->getURL();
		foreach ($this->routes as $urlPattern => $path){
			if (preg_match("~$urlPattern~", $url) ){

				//internal rout from outer
				$internalRoute = preg_replace("~$urlPattern~", $path, $url);
				echo $internalRoute;

				$segments = explode('/', $internalRoute);
				$controllerName = ucfirst(array_shift($segments).'Controller');

				$actionName = 'action'.ucfirst(array_shift($segments));
				echo 'ACTION NAME '.$actionName;

				$parameters = $segments;



				$controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
				if(file_exists($controllerFile)){
					include_once ($controllerFile);
				}

				$controllerObject = new $controllerName;
				$result = call_user_func_array(array($controllerObject, $actionName), $parameters);
				if($result != null){
					break;
				}
			}
		}


	}


}