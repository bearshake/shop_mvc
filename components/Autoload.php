<?php

function __autoload($class)
{
	$arrayPaths = array(
		'/models/',
		'/components/'
	);

	foreach ($arrayPaths as $path){
		$path = ROOT . $path . $class . '.php';
		if(is_file($path)) include_once $path;  //if exists
	}
}