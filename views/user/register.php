<?php include ROOT . '/views/layout/header.php'; ?>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-sm-offset-4 padding-right">

				<div class="signup-form">
					<h2>Register</h2>
					<form action="" method="post">
						<input type="text" placeholder="Name">
						<input type="email" placeholder="Email">
						<input type="password" placeholder="pass">
						<button type="submit" class="btn btn-default">Register</button>
					</form>
				</div>
			</div>
		</div>
	</div>

</section>

<?php include ROOT. '/views/layout/footer.php'; ?>
