<?php include ROOT. '/views/layout/header.php'; ?>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<h2>Каталог</h2>
					<div class="panel-group category-products" id="accordian"><!--category-productsr-->
						<?php foreach ($categories as $categoryItem): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="/category/<?php echo $categoryItem['id'];?>">
											<?php echo $categoryItem['name']; ?>
                                        </a>
                                    </h4>
                                </div>
                            </div>
						<?php endforeach; ?>
					</div><!--/category-products-->

				</div>
			</div>

			<div class="col-sm-9 padding-right">
				<div class="product-details"><!--product-details-->
					<div class="row">
						<div class="col-sm-5">
							<div class="view-product">
								<img src="/template/images/prod/<?php echo $product['image']?>" alt="" />
							</div>
						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<?php if(intval($product['is_new'] )> 0) echo '<img src="/template/images/home/new.png" class="newarrival" alt="" />'; else echo ''?>

								<h2><?php echo $product['name'];?></h2>
								<p>Код товара: <?php echo $product['code'];?> </p>
								<span>
                                            <span>US $59</span>
                                            <label>Количество:</label>
                                            <input type="text" value="3" />
                                            <button type="button" class="btn btn-fefault cart">
                                                <i class="fa fa-shopping-cart"></i>
                                                В корзину
                                            </button>
                                        </span>
								<p><b>Наличие:</b> <?php if(intval($product['availability'] )> 0) echo 'Есть'; else echo 'нет'?> </p>
								<p><b>Состояние:</b> <?php if(intval($product['is_new'] )> 0) echo 'Новинка'; else echo 'не новинка'?> </p>
								<p><b>Производитель:</b> <?php echo  $product['firm'] ?> </p>
							</div><!--/product-information-->
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<h5>Описание товара</h5>
							<p>
                                <?php echo $product['description']; ?>
                            </p>
							</div>
					</div>
				</div><!--/product-details-->

			</div>
		</div>
	</div>
</section>


<br/>
<br/>

<footer id="footer"><!--Footer-->
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<p class="pull-left">Copyright © 2015</p>
				<p class="pull-right">Курс PHP Start</p>
			</div>
		</div>
	</div>
</footer><!--/Footer-->



<script src="/template/js/jquery.js"></script>
<script src="/template/js/price-range.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/jquery.prettyPhoto.js"></script>
<script src="/template/js/main.js"></script>
</body>
</html>