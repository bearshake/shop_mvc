<?php


class Product {

	const SHOW_BY_DEF = 5;

	public static function getLatestProducts($count = self::SHOW_BY_DEF)
	{

		$count = intval($count);

		$db = DB::getConnection();

		$productList = array();

		$result = $db->query('SELECT id, name, price, image, is_new FROM product '
		                     .'WHERE status = "1"'
		                     . 'ORDER by id DESC '
		                     . 'LIMIT ' . $count );

		$i = 0;
		while($row = $result->fetch())
		{
			$productList[$i]['id'] = $row['id'];
			$productList[$i]['name'] = $row['name'];
			$productList[$i]['image'] = $row['image'];
			$productList[$i]['price'] = $row['price'];
			$productList[$i]['is_new'] = $row['is_new'];
			$i++;
		}
		return $productList;

	}

	public static function getProductsByCat($categoryId = false, $page = 1)
	{
		if ($categoryId)
		{
			$page = intval($page);
			$offset = ($page - 1) * self::SHOW_BY_DEF;

			$db = Db::getConnection();
			$products = array();
			$result = $db->query("SELECT id, name, price, image, is_new FROM product "

				. "WHERE status = '1' AND category_id = '$categoryId' "
				. "ORDER by id ASC "
				. "LIMIT " .self::SHOW_BY_DEF
				. ' OFFSET '.$offset
			);

			$i = 0;
			while ($row = $result->fetch()){
				$products[$i]['id'] = $row['id'];
				$products[$i]['name'] = $row['name'];
				$products[$i]['image'] = $row['image'];
				$products[$i]['price'] = $row['price'];
				$products[$i]['is_new'] = $row['is_new'];
				$i++;
			}
			return $products;
		}
	}

	public static function getProductById($id)
	{
		$id = intval($id);

		if($id){
			$db = Db::getConnection();

			$result = $db->query('SELECT * FROM product WHERE id=' .$id);
			$result->setFetchMode(PDO::FETCH_ASSOC);

			return $result->fetch();
		}

	}

	public static function getTotalProductsInCat($categoryId)
	{
		$db = Db::getConnection();

		$result = $db->query('SELECT count(id) AS count FROM product '
		                     . 'WHERE status="1" AND category_id ="'.$categoryId.'"');
		$result->setFetchMode(PDO::FETCH_ASSOC);
		$row = $result->fetch();

		return $row['count'];
	}

}