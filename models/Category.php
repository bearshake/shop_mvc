<?php

class Category
{

	/**
	 *LeftBar Categories
	 */

	public static function getCategoriesList()
	{

		$db = DB::getConnection();

		$categoryList = array();

		$result = $db->query('SELECT id, name from category ' .'ORDER by sort_order ASC');

		$i = 0;
		while($row = $result->fetch())
		{
			$categoryList[$i]['id'] = $row['id'];
			$categoryList[$i]['name'] = $row['name'];
			$i++;
		}

		return $categoryList;
	}

}