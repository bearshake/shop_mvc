<?php
/**
 * Created by PhpStorm.
 * User: BearShake
 * Date: 13.04.2018
 * Time: 19:56
 */

include_once ROOT . '/models/Category.php';
include_once ROOT . '/models/Product.php';
include_once ROOT . '/components/Pagination.php';

class CatalogController {

	public function actionIndex()
	{
		$categories = array();
		$categories = Category::getCategoriesList();

		$latestProducts = array();
		$latestProducts = Product::getLatestProducts();

		require_once ROOT . '/views/catalog/index.php';

		return true;
	}

	public function actionCategory($categoryId, $page = 1)
	{

		echo 'Category: ' .$categoryId;
		echo '<br>Page: ' .$page;

		$categories = array();
		$categories = Category::getCategoriesList();

		$categoryProducts = array();
		$categoryProducts = Product::getProductsByCat($categoryId, $page);


		$total = Product::getTotalProductsInCat($categoryId);

		$pagination = new Pagination($total, $page, Product::SHOW_BY_DEF, 'page-');

		require_once ROOT . '/views/catalog/category.php';

		return true;
	}

}