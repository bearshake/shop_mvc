<?php
/**
 * Created by PhpStorm.
 * User: BearShake
 * Date: 03.04.2018
 * Time: 17:36
 */
header('Content-type:text/html;charset=utf-8');
ini_set('display_errors', 1);
error_reporting(E_ALL);


define('ROOT', dirname(__FILE__));
require_once (ROOT.'/components/Autoload.php');

$router = new Router();
$router->run();